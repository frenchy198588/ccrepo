print("Starting the nether highway layer program")

-- error codes
-- 100 Could not bind
-- 101 Bind not a chest
-- 102 Not fuel chest
-- 103 Turtle refuel slot not empty
-- 104 Cannot dig and store item
-- 105 No empty slot for item type
-- 106 Need to dropoff materials
-- 107 Depth too far
-- 108 Cannot find rubbish chest

COAL_FUEL_VALUE = 80
MAX_REFUEL_AMOUNT = 64
MAX_FUEL_AMOUNT = 5000
CHEST_TYPE = "enderstorage:ender_chest"
INVENTORY_REFUEL_SLOT = 16
CHEST_CHECK_SLOT = 27 --Should be the last slot in a chest (27 for ender chest)
TUNNEL_SIZE = 4
TUNNEL_OBSIDIAN_SLOTS = 2
DISTANCE_BETWEEN_CHEST_MOVE = 32
LAVA = "minecraft:lava"

-- current dig distance
digDistanceFromStart = 1

-- current offset for obsidian crate
obsidianChest = nil

-- current offset for coal crate
coalChest = nil

-- current offset for rubbish crate
rubbishChest = nil

-- Relative to self, (1,1) being top left of the tunnel it's digging
curOffsetX = 1
curOffsetY = 1
curOffsetZ = 1


function getChestFront()
    local chest = peripheral.wrap("front")
    if chest == nil then
        error({code=100})
    elseif peripheral.getType("front") ~= CHEST_TYPE then
        error({code=101})
    else
        return chest
    end
end

function isChestItemType(itemType)
    local chest = peripheral.wrap("front")
    local itemType = chest.getItemDetail(CHEST_CHECK_SLOT) 
    if itemType.name == itemType and itemType.count == 64 then
        return true
    end
end

-- Function to check if the box infront is an obsidian box
-- the first item slot will always be left in there by the bots
function isObsidianChest()
    return isChestItemType("minecraft:obsidian")
end

-- Function to check if the box infront is a coal box
-- the first item slot will always be left in there by the bots
function isCoalChest()
    return isChestItemType("minecraft:coal")
end

function refuelFromChest()
    local maxFuelAllowed = MAX_FUEL_AMOUNT --turtle.getFuelLimit()
    local currentFuel = turtle.getFuelLevel()
    local fuelNeeded = maxFuelAllowed - currentFuel
    local itemsNeeded = fuelNeeded / COAL_FUEL_VALUE
    if itemsNeeded > MAX_REFUEL_AMOUNT then
        itemsNeeded = MAX_REFUEL_AMOUNT
    end

    if itemsNeeded < 0 then
        itemsNeeded = 0
    end

    -- Attempt to get items from the chest in front
    local success, chest = pcall(getChestFront)
    if success then
        print("About to grab " .. itemsNeeded .. " fuel from chest")
    else
        print("Cannot refuel, something wen't wrong")
        -- TODO: Do something like message central to let it know robot is stuck
        -- maybe find some way to reset the bot to a certain position
    end

    turtle.select(INVENTORY_REFUEL_SLOT)
    if turtle.getItemDetail() ~= nil then
        error({code=103})
    else
        turtle.suck(itemsNeeded)
        turtle.refuel()
        print(turtle.getFuelLevel())
    end
end

function digFront()
    

    while turtle.inspect() do

        local success, item = turtle.inspect()
        if success and item.name == LAVA then
            return true
        end

        print("Trying to dig item in front")
        local success = pcall(digAndStore)
        if success ~= true then
            error({code=104})
        end
    end
end

-- TODO: Need to do this more cvleverly for set size and also falling elements
-- Assume a starting block of top left
function digTunnelSection() 
    turtle.select(1)
    print("Digging tunnel section")
    for y=TUNNEL_SIZE, 1, -1 do
        for x=1, TUNNEL_SIZE do
            if pcall(digFront) ~= true then
                print("Can't dig front")
                error({code=106})
            end

            if x ~= TUNNEL_SIZE then
                moveRight(1)
            end
        end
        -- Reset the position of bot on row
        moveLeft(TUNNEL_SIZE -1)
    
        if y ~= 1 then
            moveDown()
        end
    end

    for y=1, TUNNEL_SIZE - 1 do
        moveUp()
    end
end



-- Assume a starting block of top left, facing in digging direction (outward from centre)
function placeLastHighwaySection()
    -- Select a slot  with obsidian in. There should be enough obsidian already, this is checked elsewhere
    if turtle.getItemCount(2) >= (TUNNEL_SIZE + 2) and turtle.getItemDetail(2).name == "minecraft:obsidian" then
        turtle.select(2)
    elseif turtle.getItemCount(3) >= (TUNNEL_SIZE + 2) and turtle.getItemDetail(3).name == "minecraft:obsidian" then
        turtle.select(3)
    elseif turtle.getItemCount(4) >= (TUNNEL_SIZE + 2) and turtle.getItemDetail(4).name == "minecraft:obsidian" then
        turtle.select(4)
    else
        error({code=110})
    end
    

    turnAround()
    for y=1, TUNNEL_SIZE - 2 do
        moveDown()
    end
    turtle.place()
    moveDown()
    turtle.place()

    for x=1, TUNNEL_SIZE do
        turtle.place()
        if x ~= TUNNEL_SIZE then
            moveLeft(1)
        end
    end

    moveUp()
    turtle.place()
    
    moveRight(TUNNEL_SIZE - 1)
    for y=1, TUNNEL_SIZE - 2 do
        moveUp()
    end

    turnAround()
end

function turnAround()
    turtle.turnLeft()
    turtle.turnLeft()
end

-- Build and light a nether portal
function buildNetherPortal()

end

-- Assumes that the current tunnel layer is empty
function moveRight(numTimes)
    turtle.turnRight()
    for i=1, numTimes do
        local success = turtle.forward()
        if success then
            curOffsetX = curOffsetX + 1
        end
    end
    turtle.turnLeft()
end

-- Assumes that the current tunnel layer is empty
function moveLeft(numTimes)
    turtle.turnLeft()
    for i=1, numTimes do
        local success = turtle.forward()
        if success then
            curOffsetX = curOffsetX - 1
        end
    end
    turtle.turnRight()
end

function moveUp()
    local success = turtle.up()
    if success then
        curOffsetY  = curOffsetY - 1
    end
    return success
end

function moveDown()
    local success = turtle.down()
    if success then
        curOffsetY = curOffsetY + 1
    end
    return success
end

--  U U U U
--  X - O H
--  H - C H
--  H - J H
--
--  U = Undug currently
--  H = Highway edge 
--  X = Bot location (top left)
--  O = Obsidian Chest
--  C = Coal Chest
--  J = Junk Chest (The chest where dug materials will be placed)
function pickupChests()
    moveRight(TUNNEL_SIZE - 3)
    for y=1, TUNNEL_SIZE - 2 do
        moveDown()
    end
    turtle.turnRight()

    turtle.dig()
    turtle.transferTo(13)
    moveRight(1)
    turtle.dig()
    turtle.transferTo(14)
    moveRight(1)
    turtle.dig()
    turtle.transferTo(15)
end

function placeChests()
    moveRight(TUNNEL_SIZE - 3)
    for y=1, TUNNEL_SIZE - 2 do
        moveDown()
    end
    turtle.back()
    curOffsetZ = curOffsetZ - 1

    turtle.select(13)
    turtle.turnRight()
    turtle.place()
    turtle.turnLeft()
    obsidianChest = curOffsetZ
    turtle.back()
    curOffsetZ = curOffsetZ - 1
    
    turtle.select(14)
    turtle.turnRight()
    turtle.place()
    turtle.turnLeft()
    coalChest = curOffsetZ
    turtle.back()
    curOffsetZ = curOffsetZ - 1
    
    turtle.select(15)
    turtle.turnRight()
    turtle.place()
    turtle.turnLeft()
    junkChest = curOffsetZ

    returnToTopCorner()
end

function moveToZDepth(depth)
    if depth > digDistanceFromStart then
        error({code=107})
    end

    -- Return to the top corner first
    returnToTopCorner()

    -- Now move along z axis the right way
    if curOffsetZ < depth then
        -- move forwards
        while curOffsetZ < depth do
            turtle.forward()
            curOffsetZ = curOffsetZ + 1
        end
    elseif curOffsetZ > depth then
        -- move backwards
        turnAround()
        while curOffsetZ > depth do
            turtle.forward()
            curOffsetZ = curOffsetZ - 1
        end
        turnAround()
    else
        -- no need to move, already at depth
    end
end

-- Slot allowed usage
-- P O O O
-- R R R R
-- R R R R
-- R R R R  
--
-- When working the first slot should always be reserved for pickup
-- 2, 3 and 4 should be reserved for Obsidian only
-- 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 and 16 reserved for rubbish
--
-- 
-- 
function digAndStore()
    local success, item = turtle.inspect()
    if success ~= true then
        -- return slot 5 although there's no item picked up so can pick any slot
        return 5
    end
    print(item.name)

    -- Dig the item (this is done because the item can change, i.e. stone to cobblestone)
    -- Need to figure out what happens with chests too
    turtle.select(1)
    local success = turtle.dig()
    if success then
        local item = turtle.getItemDetail(1)
        local success, freeSlot = pcall(findSlotWithSpaceForItemType, item.name)
        if success then
            print("Found slot for")
            turtle.transferTo(freeSlot)
            return freeSlot
        else
            print("Could not find a free slot")
            turtle.place()
            error({code=104})
        end
    else
        -- do nothing as there was nothing to dig (probably lava)
    end
end

-- Check the rubbish slots 5 - 16 and see if there are places the item could go
function findSlotWithSpaceForItemType(itemName)
    print("Trying to find a free slot")
    for slot = 5, 16 do
        if turtle.getItemDetail(slot) == nil then
            print("Found empty slot")
            return slot
        elseif turtle.getItemDetail(slot).name == itemName and turtle.getItemSpace(slot) >= turtle.getItemDetail(slot).count then
            print("Found slot with existing item")
            return slot
        else
            -- do nothing
        end
    end

    print("No slot")
    error({code=105})
end

function chestRun()
    dropOff()
    restockObsidian()
    refuel()
end

--TODO: IMPLEMENT REFUEL
function refuel()
    gotoChestAtDepth(coalChest)
    refuelFromChest()
    turtle.turnLeft()
    returnToTopCorner()
end

-- Restock with obsidian. Currently the bot should be empty
function restockObsidian()
    gotoChestAtDepth(obsidianChest)

    local success, chest = pcall(getChestFront)
    if success then
        -- interact with the chest
        turtle.select(2)
        turtle.suck(64)
        turtle.select(3)
        turtle.suck(64)
        turtle.select(4)
        turtle.suck(64)
    else
        error({code=108})
    end

    turtle.turnLeft()
    returnToTopCorner()
end

function isObsidianLow()
    local obsidianCount = turtle.getItemCount(2) + turtle.getItemCount(3) + turtle.getItemCount(4)
    if obsidianCount < ((TUNNEL_SIZE + 2)*3) then
        return true
    else
        return false
    end
end

function isFuelLow()
    local curFuelLevel = turtle.getFuelLevel()
    if curFuelLevel < (digDistanceFromStart * 10) then
        return true
    else
        return false
    end
end

function layHighway()
    placeChests()
    chestRun()
    moveToZDepth(digDistanceFromStart)
    while true do
        --TODO: Check the obsidian level and fuel levels and decide if either need
        -- to be topped up. If they do, then may aswell do a junk drop off and
        -- top up. We'll call this a chest run.
        if isObsidianLow() or isFuelLow() then
            chestRun()
            moveToZDepth(digDistanceFromStart)
        end

        success, error = pcall(digTunnelSection)
        if success ~= true then
            chestRun()
            moveToZDepth(digDistanceFromStart)
        else
            turtle.forward()
            placeLastHighwaySection()
            digDistanceFromStart = digDistanceFromStart + 1
            curOffsetZ = curOffsetZ + 1
        end
    end
end

function digItem()
    local success, slot = pcall(digAndStore)
    if success then
        print("Can dig")
        --turtle.dig()
        --turtle.transferTo(slot)
        -- Drop any remains in case there were multiple items picked up
        --turtle.drop()
    else 
        print("Can't dig")
        error({code=104})
    end
end


--
--  X
--  - - - -
--  - - - -
--  O - C O
--  O O O O
--
-- Assume a starting position of top left
function gotoChestAtDepth(depth)
    returnToTopCorner()
    moveToZDepth(depth)
    moveRight(TUNNEL_SIZE - 3)
    for y=1, TUNNEL_SIZE - 2 do
        moveDown()
    end
    turtle.turnRight()
end

function dropOff()
    print("Need to drop off junk")
    gotoChestAtDepth(junkChest)

    local success, chest = pcall(getChestFront)
    if success then
        -- interact with the chest
        -- Empty slots 1 through 16 into the chest (we'll pick up obsidian again)
        for slot=1, 16 do
            turtle.select(slot)
            turtle.drop()
        end
    else
        error({code=108})
    end

    turtle.turnLeft()
    returnToTopCorner()
end

function returnToTopCorner()
    while curOffsetY > 1 do
        moveUp()
    end
    moveLeft(curOffsetX - 1)
end

layHighway()