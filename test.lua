print("Starting program")

maxFuelLevel = 100
fuelLevel = 100

mon = peripheral.wrap("top")

mon.setTextScale(0.5)
x, y = mon.getSize()
maxBarHeight = y - 4

function displayFuelLevel()
    
    mon.setBackgroundColor(colors.gray)
    mon.clear()

    incrementHeight = maxBarHeight / maxFuelLevel
    barHeight = incrementHeight * fuelLevel
    if barHeight > maxBarHeight then
        barHeight = maxBarHeight
    elseif barHeight < 0 then
        barHeight = 0
    end

    paintutils.drawFilledBox(3, y - 3, 5, (y - 3 - barHeight + 1), colors.green)

    mon.setBackgroundColor(colors.gray)
    mon.setCursorPos(3, y - 1)
    mon.write(math.floor(fuelLevel))
end

while fuelLevel > 0 do
    displayFuelLevel()
    fuelLevel = fuelLevel - 5
    sleep(1)
end

displayFuelLevel()
