# Induction matrix montior

mon = peripheral.wrap("back")
mon.setTextScale(2)
mon.clear()


induction_matrix = peripheral.wrap("bottom")

function format_int(number)
    local i, j, minus, int, fraction = tostring(number):find('([-]?)(%d+)([.]?%d*)')
  
    -- reverse the int-string and append a comma to all blocks of 3 digits
    int = int:reverse():gsub("(%d%d%d)", "%1,")
  
    -- reverse the int-string back remove an optional comma and put the 
    -- optional minus and fractional part back
    return minus .. int:reverse():gsub("^,", "") .. fraction
end


while true do
    curEnergy = induction_matrix.getEnergy()
    maxEnergy = induction_matrix.getEnergyCapacity()
    percentFull = 100 * (curEnergy / maxEnergy)
    displayEnergy = format_int(curEnergy)
    mon.clear()
    mon.setCursorPos(1, 1)
    mon.write(string.format("%.2f %%", percentFull))
    mon.setCursorPos(1, 2)
    mon.write(displayEnergy .. " RF")
    sleep(2)
end
